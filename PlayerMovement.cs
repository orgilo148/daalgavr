﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody r;
    private Vector3 screendBounds;
    public float clampRadius;
    public int maxChildCount;
    private GameObject saveCoinGO;
    void Start()
    {
        screendBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        r = GetComponent<Rigidbody>();
        r.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
        r.useGravity = false;

    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            float h =0.2f * Input.GetAxisRaw("Mouse X");
            transform.Translate(h, 0, 0);
            Vector3 viewPos = transform.position;
            viewPos.x = Mathf.Clamp(viewPos.x, -clampRadius , clampRadius);
            transform.position = viewPos;
        }

        if (transform.childCount > maxChildCount)
        {
            Transform[] allchildren = GetComponentsInChildren<Transform>();
            for(int i = maxChildCount; i < allchildren.Length; i++)
            {
                Destroy(allchildren[i].gameObject);
            }
        }

        Transform[] allCoins = GetComponentsInChildren<Transform>();
        

        if (SC_Grnd.instance.coins == 0)
        {
            for(int i =1; i<allCoins.Length;i++)
            {
                allCoins[i].parent = saveCoinGO.transform;   
                DetachCoin(allCoins[i].gameObject, saveCoinGO);
            }
        }
        else
        {     
            foreach (Transform coin in allCoins)
            {
                AttachCoin(coin.gameObject);
            }
        }

    }



    private void OnCollisionEnter(Collision collision)
    {
  
        if(collision.gameObject.tag == "Finish")
        {
            SC_Grnd.instance.gameOver = true;
        }

    }

   
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Coin")
        {
            
            SC_Grnd.instance.coins += 0.5f;
            Debug.Log(SC_Grnd.instance.coins);
          
            {
                GameObject copyCoin = Instantiate(other.gameObject, transform);
                copyCoin.transform.localPosition = new Vector3(other.transform.position.x, 1, -1.5f);
                copyCoin.transform.localScale = new Vector3(0.3f, 0.03f, 0.3f);
                copyCoin.AddComponent<BoxCollider>();
                copyCoin.GetComponent<BoxCollider>().isTrigger = false;
                copyCoin.AddComponent<Rigidbody>().useGravity = false; ;
                Destroy(copyCoin.GetComponent<Animator>());
            }
            other.gameObject.SetActive(false);
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
      
        if (other.gameObject.tag == "CoinSave")
        {
            SC_Grnd.instance.savedCoins += (int)SC_Grnd.instance.coins;
            SC_Grnd.instance.coins = 0;
            saveCoinGO = other.gameObject;
           //other.gameObject.SetActive(false);
        }
    }

    private void AttachCoin(GameObject coin)
    {
        
        Rigidbody rCoin = coin.GetComponent<Rigidbody>();
        Vector3 direction = transform.position - coin.transform.position;
        rCoin.AddForce(direction);
    }

    private void DetachCoin(GameObject coin, GameObject saveCoinGO)
    {
        Rigidbody rCOin = coin.GetComponent<Rigidbody>();
        Vector3 direction = saveCoinGO.transform.position - coin.transform.position;
        rCOin.AddForce(-direction);
    }
}
