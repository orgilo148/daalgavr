﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SC_Grnd : MonoBehaviour
{
    public Camera mainCamera;
    public Transform startPoint;
    public SC_PlatformTIle tilePrefab;
    public float movingspeed;
    public int tilestToPreSpawn ;
    public int tilesWithoutObstacles;
    public int tilesBetweenCoins;
    public int tilesBetweenCoinSave;
   
    public Button startButton;

    private int tilesBetweenCoinSaveTmp;
    private int tilesBetweenCoinsTmp;
    
    

    [SerializeField] private Text scoreText;
    [SerializeField] private Text startText;
    [SerializeField] private Text restartText;
    [SerializeField] private Text coinText;
    [SerializeField] private Text coinSaveText;
    List<SC_PlatformTIle> spawnedTiles = new List<SC_PlatformTIle>();
    public bool gameOver = false;
    static bool gameStarted = false;
    float score = 0;
    public float coins = 0;
    public int savedCoins = 0;

    public static SC_Grnd instance;

    // Start is called before the first frame update
    void Start()
    {
        restartText.gameObject.SetActive(false);

        instance = this;

        Vector3 spawnPosition = startPoint.position;
        int tilesWithNoObstaclesTmp = tilesWithoutObstacles;
        for( int i = 0; i< tilestToPreSpawn; i++)
        {
            spawnPosition -= tilePrefab.startpoint.localPosition;
            SC_PlatformTIle spawnedTile = Instantiate(tilePrefab, spawnPosition, Quaternion.identity) as SC_PlatformTIle;
            if (tilesWithNoObstaclesTmp > 0)
            {
                spawnedTile.DisableAllObstacle();
                spawnedTile.DisableAllCoin();
                tilesWithNoObstaclesTmp--;
            }
            else
            {
                spawnedTile.EnableRandomObstacle();
                spawnedTile.EnableRandomCoin();
            }

            spawnPosition = spawnedTile.endpoint.position;
            spawnedTile.transform.SetParent(transform);
            spawnedTiles.Add(spawnedTile);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
    
 
        if(!gameOver && gameStarted)
        {
            startButton.gameObject.SetActive(false);
            startText.gameObject.SetActive(false);

            transform.Translate(-spawnedTiles[0].transform.forward * Time.deltaTime * (movingspeed), Space.World);
            scoreText.text =""+ (int) score;
           // coinText.text = "" + (int) coins;
            coinSaveText.text = "" + savedCoins;
        }
        if (mainCamera.WorldToViewportPoint(spawnedTiles[0].endpoint.position).z < 0)
        {
            score += 1;
            SC_PlatformTIle tileTmp = spawnedTiles[0];
            spawnedTiles.RemoveAt(0);
            tileTmp.transform.position = spawnedTiles[spawnedTiles.Count - 1].endpoint.position - tileTmp.startpoint.localPosition;
            tileTmp.EnableRandomObstacle();
            tileTmp.EnableRandomCoin();

            
            if (tilesBetweenCoinSaveTmp == 0)
            {
                tilesBetweenCoinSaveTmp = tilesBetweenCoinSave;
                tileTmp.EnableCoinSave();
            }
            else
            {
                tilesBetweenCoinSaveTmp--;
                tileTmp.DisableCoinSave();
            }

            spawnedTiles.Add(tileTmp);
        }

        if(gameOver || !gameStarted)
        {
            if (gameOver)
            {
                StartCoroutine(GameOverSequence());
            }
            else if(!gameStarted)
            {
                GameStartSeq();
            }
        }
        
    }

    private IEnumerator GameOverSequence()
    {

        yield return new WaitForSeconds(1.0f);
        scoreText.gameObject.SetActive(false);
        startButton.GetComponentInChildren<Text>().text = "Restart";
        startButton.gameObject.SetActive(true);
        restartText.text = "Game Over!\nScore:" + (int) score;
        restartText.gameObject.SetActive(true);
    }

    private void GameStartSeq()
    {
        startButton.GetComponentInChildren<Text>().text = "Start";
        startButton.gameObject.SetActive(true);
    }
    
    public void StartOnClick()
    {
        if (gameOver)
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
        else
        {
            gameStarted = true;
        }
    }

}