﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SC_PlatformTIle : MonoBehaviour
{
    public Transform endpoint;
    public Transform startpoint;
    public GameObject[] obstacles;
    public GameObject[] coins;
    public GameObject[] coinSave;
    // Start is called before the first frame update
    public void EnableRandomObstacle()
    {
        DisableAllObstacle();

        System.Random random = new System.Random();
        int randomNumber = random.Next(0, obstacles.Length);
        obstacles[randomNumber].SetActive(true);

    }


    public void DisableAllObstacle()
    {
        for(int i =0; i<obstacles.Length; i++)
        {
            obstacles[i].SetActive(false);
        }

    }

    public void EnableRandomCoin()
    {
        DisableAllCoin();

        for(int i =0; i<coins.Length; i++)
        {
           System.Random random = new System.Random();
           int randomNumber = random.Next(0, coins[i].transform.childCount);
           Transform childCoin = coins[i].transform.GetChild(randomNumber);
           childCoin.gameObject.SetActive(true);
        }
        
        

    }

    public void DisableAllCoin()
    {
        for (int i = 0; i < coins.Length; i++)
        {
            for (int j=0; j < coins[i].transform.childCount; j++)
            {
                Transform children = coins[i].transform.GetChild(j);
                children.gameObject.SetActive(false);
            }
        }

    }

    public void EnableCoinSave()
    {
        DisableCoinSave();
        System.Random random = new System.Random();
        int randomNumber = random.Next(0, coinSave.Length);
        coinSave[randomNumber].SetActive(true);
    }

    public void DisableCoinSave()
    {
        for (int i = 0; i < coinSave.Length; i++)
        {
            coinSave[i].SetActive(false);
        }


    }
}
